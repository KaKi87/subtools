# subtools

Advanced SRT subtitles parser.

## Getting started

### Prerequisites

- NodeJS
- NPM
- Yarn

### Install

From [npm](https://www.npmjs.com/package/subtools)

`yarn add subtools`

or

`npm i subtools`

### Use

#### The SRT format

![srt](https://git.kaki87.net/KaKi87/subtools/raw/branch/master/srt.jpg)

Source : [3PlayMedia](https://www.3playmedia.com/2017/03/08/create-srt-file/)

#### Import module

```js
const subtools = require('subtools');
```

#### Import subtitles

##### From variable

```js
const mySubtitles = new subtools.Sub(body);
```

- `body` *string* - Your subtitles in SRT format

##### From file

```js
const mySrtFile = new subtools.SubFile(file, encoding);
```

- `file` *string* - Path to *.srt file
- `encoding` *string* (optional) - File encoding
<br>Default : `utf-8`

#### `Sub` class

- `subtitles` *array* - List of `Subtitle` instances
- `sentences` *array* - List of `Sentence` instances
- `getSubtitle` *function* - Get subtitle
	- `key` *integer* - Subtitle key
- `print` *function* - Export subtitles to variable
- `write` *function* - Export subtitles to file
	- `file` *string* - Path to file

#### `SubFile` class (inherits from `Sub`)

`Sub` class properties and methods +
- `file` *string* - Path to *.srt file
- `write` *function* - Export subtitles to file
	- `file` *string* - Path to file
	<br>Default : initial file path

#### `Subtitle` class

- `key` *integer* - SRT key
- `begin` *time* - `Date` instance matching SRT begin timecode
- `end` *time* - `Date` instance matching SRT end timecode
- `text` *string* - SRT text, newlines removed
- `sentences` *array* - List of `Sentence` partially or entirely present in subtitle
- `getPrev` *function* - Get previous subtitle
- `getNext` *function* - Get next subtitle
- `setText` *function* - Change subtitle text and auto adapt timecodes
- `addText` *function* - Append text to subtitle body
	- `position` *integer* - Substring index
- `removeText` *function* - Remove text from subtitle body
	- `start` *integer* - Substring start index
	- `end` *integer* - Substring end index
- `print` *function* - Export individual subtitle item to variable

#### `Sentence` class

- `subtitles` *array* - List of `Subtitle` partially or entirely containing the sentence
- `text` *string* - Sentence body
- `position` *integer* - Substring index of sentence in subtitle text
- `setText` *function* - Change sentence text and auto-update related `Subtitle` instances
	- `text` *string*

### Planned features

I designed this module to make subtitle translations easier with sentence context.
<br>However, I'll add more features that common subtitles tools modules have.

## Changelog

* `1.0.1` (2019-05-11) • Initial release
