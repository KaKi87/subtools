const
	fs = require('fs'),
	Lexed = require('lexed').Lexed;

const
	Subtitle = require('./Subtitle'),
	Sentence = require('./Sentence');

module.exports = class Sub {
	constructor(body){
		this.subtitles = [];
		this.sentences = [];
		const items = body.replace(/\r/g, '').trim().split('\n\n');
		let
			fullText = '',
			lastSentencesCount = 0;
		for(let i = 0; i < items.length; i++){
			const
				item = items[i],
				lines = item.split('\n'),
				key = parseInt(lines[0]),
				timecodes = lines[1].split(' --> '),
				dateStrings = timecodes.map(t => `1970-01-01T${t.replace(',', '.')}Z`),
				beginDate = new Date(dateStrings[0]),
				endDate = new Date(dateStrings[1]),
				text = lines.slice(2).join('\n').replace(/\n/g, ' '),
				subtitle = new Subtitle(key, beginDate, endDate, text, () => this.getSubtitle(key - 1), () => this.getSubtitle(key + 1));
			fullText += text + ' ';
			const
				sentences = new Lexed(fullText).sentenceLevel(),
				currentSentencesCount = sentences.length,
				diff = currentSentencesCount - lastSentencesCount,
				currentSentenceText = sentences[lastSentencesCount - 1],
				lastSentence = this.sentences.slice(-1)[0];
			if(lastSentence && lastSentence.text !== currentSentenceText){
				lastSentence.text = currentSentenceText;
				lastSentence.subtitles.push(subtitle);
				subtitle.sentences.push(lastSentence);
			}
			if(diff){
				lastSentencesCount = currentSentencesCount;
				const newSentencesText = sentences.slice(-diff);
				const newSentences = newSentencesText.map((text, index) => new Sentence(
					subtitle,
					text,
					newSentencesText.slice(0, index).join(' ').length + (index > 0)
				));
				subtitle.sentences.push(...newSentences);
				this.sentences.push(...newSentences);
			}
			this.subtitles.push(subtitle);
		}
	}
	getSubtitle(key){
		return this.subtitles[key - 1];
	}
	print(){
		return this.subtitles.map(subtitle => subtitle.print()).join('\n\n');
	}
	write(file){
		fs.writeFileSync(file, this.print());
	}
};
