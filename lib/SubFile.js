const fs = require('fs');

const Sub = require('./Sub');

module.exports = class SubFile extends Sub {
	constructor(file, encoding = 'utf-8'){
		super(fs.readFileSync(file, { encoding }));
		this.file = file;
	}
	write(file = this.file){
		super.write(file);
	}
};
