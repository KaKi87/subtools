const autoSplit = (string, proportions, max = Infinity) => {
	let n = 0;
	const
		p = proportions,
		a = string.split(' '),
		l = string.length,
		b = [];
	for(let i = 0; i < a.length; i++){
		if(b.length === 0){
			b.push(a[i]);
		}
		else if((a[i].length + b[n].length) / l <= p[n]){
			b[n] += ' ' + a[i];
		}
		else if(b.length === max){
			return autoSplit(string, proportions.map(p => p * 1.1), max);
		}
		else {
			b.push(a[i]);
			n++;
		}
	}
	return b;
};

module.exports = class Sentence {
	constructor(subtitle, text, position){
		this.subtitles = [ subtitle ];
		this.text = text;
		this.position = position;
	}
	setText(text){
		const
			subtitlesText = this.subtitles.map(subtitle => subtitle.text),
			proportions = subtitlesText.map(text => text.length / subtitlesText.join('').length),
			sentenceParts = autoSplit(text, proportions, this.subtitles.length);
		this.subtitles.forEach((subtitle, index) => {
			const start = index === 0 ? this.position : 0;
			subtitle.removeText(start, start + this.text.length);
			subtitle.addText(sentenceParts[index], start);
		});
		this.text = text;
	}
};
