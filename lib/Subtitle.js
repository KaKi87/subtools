const autoWrap = string => string.split(' ').reduce((a, b) => `${a}${(a.split('\n').slice(-1)[0].length + b.length > 40) ? '\n' : ' '}${b}`);

const dateToSrtString = date => date.toISOString().split('T')[1].slice(0, -1).replace('.', ',');

module.exports = class Subtitle {
	constructor(key, begin, end, text, prev, next){
		this.key = key;
		this.begin = begin;
		this.end = end;
		this.text = text;
		this.sentences = [];
		this.getPrev = prev;
		this.getNext = next;
	}
	setText(newText){
		const
			newTextLength = newText.length,
			newTextWordCount = newText.split(' ').length,
			next = this.getNext(),
			nextBeginTime = next ? next.begin : null;
		let newEndTime = new Date(this.begin.getTime() + newTextWordCount * 375);
		if(nextBeginTime && newEndTime > nextBeginTime){
			newEndTime = nextBeginTime;
			process.emitWarning(`[${this.key}] Time / word ratio below recommended level`);
		}
		if(newTextLength > 80)
			process.emitWarning(`[${this.key}] Text exceeds 80 characters recommended limit.`);
		this.text = autoWrap(newText);
		this.end = newEndTime;
	}
	addText(text, position){
		const currentText = this.text;
		this.setText(currentText.slice(0, position) + text + currentText.slice(position));
	}
	removeText(start, end){
		const currentText = this.text;
		this.setText(currentText.slice(0, start) + currentText.slice(end));
	}
	print(){
		return `${this.key}\n${dateToSrtString(this.begin)} --> ${dateToSrtString(this.end)}\n${this.text}`;
	}
};
